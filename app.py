import os
import logging

from contributions.render_html import create_graph
from flask import Flask
from flask import send_from_directory

app = Flask(__name__, static_url_path='')

@app.route('/contributions/static/style.css')
def send_js():
    return send_from_directory('contributions/static/', "style.css")

@app.route("/")
def generate_graphs():
  log_dir = "/logs"
  log_files = [os.path.join(log_dir, log_file) for log_file in
               os.listdir(log_dir)]

  print("log_files", log_files)
  logging.info(log_files)
  page_html = create_graph(log_files)
  return page_html

if __name__ == "__main__":
    app.run(host='0.0.0.0')
