## Github Style Contributions Graph

This is a Dockerised version of [https://github.com/insectatorious/contributions-graph](https://github.com/insectatorious/contributions-graph).

## Requirements

- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/) [Optional - see below]

## Usage

### Create a `logs` folder
Create a folder called `logs` and place a text file for each metric that needs to be graphed in it.
For example, each git repository can be in a separate file.
Each text file contains records for each day, with the number of contributions for that day, with a space between the date and the value:

    YYYY-MM-DD value

For example, 
        
    2017-09-08 4
    2017-09-07 2
    2017-09-06 4
    2017-09-05 1
    2017-09-01 1
    2017-08-31 4
    2017-08-29 4
    2017-08-17 3
    2017-08-16 3
    2017-08-14 1

To generate this file from a git repository, run 

    mkdir /tmp/logs
    git log --pretty='format:%aI' | cut -c -10 | uniq -c | awk '{ print $2 " " $1}' > /tmp/logs/${PWD##*/}

which will place a text file in the required format in `/tmp/logs/` with the filename as the folder name (which should 
hopefully be the repository name).
 
Other notes:

*   As with Python source code, anything after a `#` is ignored and treated as a comment.
*   One date/value pair per line.
*   Blank lines are fine.

### Run (without Docker Compose)

    export LOGS_DIR=/tmp/logs
    export HOST_PORT=5000
    docker run -v $LOGS_DIR:/logs -p $HOST_PORT:5000 insectatorious/contributions-graph
   
where `LOGS_DIR` is the path to the `logs` folder as described above and `HOST_PORT` is the port on the host machine
that is available.

Once complete, navigate to [http://localhost:5000](http://localhost:5000), assuming `HOST_PORT=5000`.

### Run (with Docker Compose)

Clone the source repository and run

    cd $LOGS_DIR
    docker-compose up
   
and navigate to [http://localhost:5000](http://localhost:5000).
To change the port, open `docker-compose.yaml` and edit the `ports` entry as needed.